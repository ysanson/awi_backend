FROM node:12

# Create app directory
WORKDIR /usr/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY .babelrc ./
COPY src/ src/

RUN npm install && npm run build && npm prune --production && mv src/images build/ && rm -rf src/
# If you are building your code for production
#RUN npm ci --only=production

EXPOSE 3000
CMD [ "npm", "run", "prod" ]