import { google } from 'googleapis';

/**
 * Creates a google Oauth object.
 * @return {google.auth.OAuth2} the oauth object
 */
function createConnection() {
    return new google.auth.OAuth2(
        process.env.CLIENT_ID,
        process.env.CLIENT_SECRET,
        // the site address to redirect after
        process.env.REDIRECT_URL,
    );
}

// give access to an api
const scopes= ['https://www.googleapis.com/auth/drive'];

/**
 * Gets the connection URL
 * @param {google.auth.OAuth2} auth the OAuth object
 * @return {String} the connection URL
 */
function getConnectionUrl(auth) {
    return oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes,
        prompt: 'consent',
    });
}

/**
 * Gets the google drive API
 * @param {google.auth.OAuth2} auth the OAuth object
 * @return {*} the drive API
 */
function getGoogleDriveApi(auth) {
    return google.drive({ version: 'v3', auth });
}

/**
 *  Create the url to access to the google drive connection page. Send this url to the front to have the page.
 * @return {String} the constant page URL
 */
function createGoogleConsentPageUrl() {
    const auth = createConnection();
    return getConnectionUrl(auth);
}

/**
 * Get the access to google drive api
 * @param {String} code code sent by google after OAuth authentication
 */
async function getGoogleAccountFromCode(code) {
    // retrieve the tokens
    const data = await auth.getToken(code);
    // save tokens somewhere
    const tokens = data.tokens;
    const auth = createConnection();
    auth.setCredentials(tokens);
    const drive = getGoogleDriveApi(auth);
    // do the synchro with a folder with the drive api
}


export default {
    createConnection,
    getConnectionUrl,
    getGoogleDriveApi,
    createGoogleConsentPageUrl,
    getGoogleAccountFromCode,
};
