// const { drive_v3 } = require('googleapis')
import async from 'async';

/**
 * Searches folder to sync in the drive
 * @param {*} drive The drive access
 * @param {String} folderName the folder name
 * @return {Sting} the folder ID
 */
function searchFolderToSynchronised(drive, folderName) {
    // split to have just the name of the folder
    const folderNameSplit = folderName.split('/');
    const folderToSynchronised = folderNameSplit[folderNameSplit.length-1];

    const findFolders= [];
    let pageToken = null;

    // search the folder in the user drive
    async.doWhilst(function(callback) {
        drive.files.list({
            q: 'mimeType = \'application/vnd.google-apps.folder\' and name = \'' + folderToSynchronised + '\'',
            fields: 'nextPageToken, files(id, name)',
            spaces: 'drive',
            pageToken: pageToken,
        }, function(err, res) {
            if (err) {
                console.error(err);
                callback(err);
            } else {
                res.files.forEach(function(file) {
                    findFolders.push({ name: file.name, parent: file.parent, id: file.id });
                });
                pageToken = res.nextPageToken;
                callback();
            }
        });
    });

    let finalFolder = null;
    // check if there are several retrieved folders
    if (findFolders.length > 1) {
        // check the parent's folder
        const parent = folderNameSplit[folderNameSplit.length-2];
        finalFolder = findFolders.filter((f) => f.parent == parent).id;
    } else finalFolder = findFolders[0].id;

    // return the folder id
    return finalFolder;
}

export default {
    searchFolderToSynchronised,
};
