import { Router } from 'express';
import courseController from '../controllers/courseController';

// eslint-disable-next-line new-cap
const router = Router();

router.post('/', (req, res) => {
    const userId = req.body.userId;
    const courseName = req.body.courseName;
    const description = req.body.description;
    const courseImage = req.body.courseImage;
    const synchro = req.body.synchro;
    const driveFolder = req.body.driveFolder;
    const access = req.body.access;
    const password = req.body.password;
    const code = req.body.code;

    courseController.newCourse(userId, courseName, description, courseImage, synchro, driveFolder, access, password, code)
        .then( (state) => res.status(201).send(state))
        .catch( (err) => res.status(500).json(err));
});

router.get('/', (req, res) => {
    const creatorId = req.query.creatorId;
    courseController.getCourses(null, creatorId)
        .then( (result) => res.status(result[0]).send(result[1]))
        .catch( (err) => res.status(500).json(err));
});

router.get('/:courseId', (req, res) => {
    const courseId = req.params.courseId;
    courseController.getCourses(courseId)
        .then( (result) => res.status(result[0]).send(result[1]))
        .catch( (err) => res.status(500).json(err));
});

router.patch('/:courseId', (req, res) => {
    const courseId = req.params.courseId;
    const newData = req.body;
    courseController.updateCourse(courseId, newData)
        .then( (result) => res.status(result[0]).send(result[1]))
        .catch( (err) => res.status(500).json(err));
});

router.delete('/:courseId', (req, res) => {
    const courseId = req.params.courseId;
    courseController.deleteCourse(courseId)
        .then( (result) => res.status(result[0]).send(result[1]))
        .catch( (err) => res.status(500).json(err));
});


export default router;
