import { Router } from 'express';

import course from './courseRouter';
import video from './videoRoutes/';

// eslint-disable-next-line new-cap
const router = Router();

router.get('/api-status', (req, res) =>
    res.json({ status: 'running' }),
);

router.use('/course', course);
router.use('/video', video);

export default router;
