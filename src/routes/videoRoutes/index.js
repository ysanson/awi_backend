import { Router } from 'express';
import sttRoutes from './sttRoutes';
import videoRoutes from './videoRoutes';

const router = Router();

router.use('/stt', sttRoutes);
router.use('/', videoRoutes);

export default router;
