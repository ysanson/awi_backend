import { Router } from 'express';
import sttController from '../../controllers/videoControllers/sttController';

// eslint-disable-next-line new-cap
const router = Router();

router.post('/', (req, res) => {
    const videoId = req.query.videoId || null;
    const content = req.query.content || null;
    sttController.handleVideo(videoId, content)
        .then( (response) => res.status(response[0]))
        .catch((e) => res.status(500).json({ 'error': e }));
});

router.get('/:videoId', (req, res) => {
    const videoId = req.params.videoId || null;
    sttController.getVideoTranscript(videoId)
        .then( (response) => res.status(response[0]).json(response[1]))
        .catch((e) => res.status(500).json({ 'error': e }));
});

export default router;
