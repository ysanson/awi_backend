import CourseModel from '../models/courseModel';
import mongoose from 'mongoose';

/**
 * Creates a new course in the database
 * @param {Number} userId the user ID
 * @param {String} name the user name
 * @param {String} desc the course description
 * @param {String} img the image in base64
 * @param {Boolean} synchro the sync
 * @param {*} driveFolder the drive folder
 * @param {*} access the access
 * @param {*} password the password
 * @param {*} videos the videos of the course.
 * @return {Promise} true if created, error otherwise
 */
async function createCourse(userId, name, desc, img, synchro, driveFolder, access, password, videos) {
    const course = new CourseModel({
        creatorId: userId,
        courseName: name,
        description: desc,
        image: img,
        access: access,
        password: password,
        isSynchro: synchro,
        driveFolder: driveFolder,
        videos: videos,
    });

    // console.log(mongoose.connection.readyState)

    return await course.save();
}

/**
 * Gets a course or specific courses with given parameters.
 * @param {JSON} params The params to srarch with.
 * @return {Array<JSON>} The resulting courses.
 */
async function getCourses(params) {
    return await CourseModel.find(params);
}

/**
 * Update courses based on its id.
 * @param {JSON} courseId the params to update many courses
 * @param {JSON} newData the new data.
 */
async function updateCourse(courseId, newData) {
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        return await CourseModel.findByIdAndUpdate(courseId, newData, { 'new': true });
    } else {
        throw new Error('The ID is not valid');
    }
}

/**
 * Deletes a course based on its ID.
 * @param {Number} courseId The course ID to delete.
 * @return {Promise<void>} The completion.
 */
async function deleteCourse(courseId) {
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        return await CourseModel.findByIdAndDelete(courseId);
    } else {
        throw new Error('The ID is not valid');
    }
}

export default {
    createCourse,
    getCourses,
    updateCourse,
    deleteCourse,
};
