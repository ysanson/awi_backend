import 'dotenv/config';
import mongoose from 'mongoose';

/**
 * Connects to the mongoose database.
 */
function connect() {
    mongoose.connect(process.env.MONGO_URL, {
        poolSize: 100,
        socketTimeoutMS: 0,
        keepAlive: true,
        reconnectTries: 30,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
    });
    const connection = mongoose.connection;
    connection.on('error', console.error.bind(console, 'MongoDB connection error'));
}

export default connect;

