import models from '../models';

/**
 * Inserts a video transcript in the database.
 * @param {Number} videoId the video ID in the database
 * @param {JSON} transcript the transcript
 * @return {JSON} The updated document
 */
async function insertVideoTranscript(videoId, transcript) {
    return await models.videoModel.findOneAndUpdate({ '_Id': videoId }, { 'transcript': transcript }, { 'new': true });
}

/**
 * Gets the video transcript.
 * @param {Number} videoId the video ID.
 * @return {Array<JSON>} The transcript
 */
async function getVideoTranscript(videoId) {
    return await models.videoModel.findOne({ '_Id': videoId });
}

export default {
    insertVideoTranscript,
    getVideoTranscript,
};
