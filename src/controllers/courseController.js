import fs from 'fs';
import courseDAO from '../database/courseDAO';
import googleDriveConnection from '../utils/GoogleDriveConnection';
import searchFilesIntoDrive from '../utils/searchFilesIntoDrive';

/**
 * Create a new course.
 * @param {Number} userId user id
 * @param {String} courseName course name
 * @param {String} desc course description
 * @param {String} courseImage course image (it could by null)
 * @param {Boolean} synchro is the course is synchronized with a drive folder
 * @param {String} driveFolder  drive folder (could be null is not synchronized)
 * @param {Boolean} access public or private course
 * @param {String} password course password (could be null is access is public)
 * @param {String} code code given by google after OAuth
 * @return {String} state of the insertion in the DB
 */
async function newCourse(userId, courseName, desc, courseImage, synchro, driveFolder, access, password, code) {
    // check if an image is given for the course
    let img = null;
    if (courseImage == null) img = fs.readFileSync('src/images/defaultCourseImg.png').toString('base64');
    else img = courseImage;

    // heck if the course is synchro to a drive folder
    const videos = [];
    if (synchro) {
        const drive = googleDriveConnection.getGoogleAccountFromCode(code);
        searchFilesIntoDrive.searchFolderToSynchronised(drive, driveFolder);
        // TODO the synchronization with the folder
        // videos =
    }
    // save the course in the DB
    return await courseDAO.createCourse(userId, courseName, desc, img, synchro, driveFolder, access, password, videos);
}

/**
 * Gets the courses from the database.
 * @param {Number} courseId The course ID.
 * @param {Number} creatorId The creator ID.
 */
async function getCourses(courseId = null, creatorId = null) {
    let courses = [];
    if (courseId != null) courses = await courseDAO.getCourses({ '_id': courseId });
    else if (creatorId != null) courses = await courseDAO.getCourses({ 'creatorId': creatorId });
    else courses = await courseDAO.getCourses({});
    if (courses.length == 0) return [404, null];
    else if (courseId != null) return [200, courses[0]];
    else return [200, courses];
}

/**
 * Updates a specific course.
 * @param {Number} courseId the course ID
 * @param {*} newContent The new content
 * @return {Array<Number, JSON>} the result to send to the client.
 */
async function updateCourse(courseId = null, newContent) {
    if (courseId == null) return [400, { 'error': 'ID missing' }];
    else {
        const newDoc = await courseDAO.updateCourse(courseId, newContent);
        return [200, newDoc];
    }
}

/**
 * Deletes a course based on its id.
 * @param {Number} courseId The course ID.
 * @return {Array<Number, JSON>} the result to send to the client.
 */
function deleteCourse(courseId = null) {
    if (courseId == null) return [400, { 'error': 'ID missing' }];
    else {
        return courseDAO.deleteCourse(courseId)
            .then( () => [204, null])
            .catch( (err) => [500, { 'error': err }]);
    }
}

export default {
    newCourse,
    getCourses,
    updateCourse,
    deleteCourse,
};
