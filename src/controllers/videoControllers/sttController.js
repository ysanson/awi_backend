import speech from '@google-cloud/speech';
import sttDAO from '../../database/sttDAO';

/**
 * Calls the API of Google to convert speech to text
 * @param {String} content The video in String base 64
 * @param {String} language of the video, in BCP-47 language code.
 * @return {Promise<JSON>} the transcription in an array of JSON { word: String, startTime: Int, endTime: Int }.
 */
async function getSTT(content, language = 'en-US') {
    // Imports the Google Cloud client library
    const client = new speech.SpeechClient();
    const audio = {
        content: content,
    };
    const config = {
        encoding: 'LINEAR16',
        sampleRateHertz: 16000,
        languageCode: language,
        model: 'video',
    };
    const request = {
        audio: audio,
        config: config,
    };
    const [operation] = await client.longRunningRecognize(request);

    const [response] = await operation.promise();
    response.results.map((result) => {
        console.log(`Transcription: ${result.alternatives[0].transcript}`);
        result.alternatives[0].words.map((wordInfo) => {
            const startSecs = wordInfo.startTime.seconds;
            const endSecs = wordInfo.endTime.seconds;

            console.log(`Word: ${wordInfo.word}`);
            console.log(`\t ${startSecs} secs - ${endSecs} secs`);
            return { 'line': wordInfo.word, 'beginningTimestamp': startSecs };
        });
    });
}

/**
 * Handles the speech to text request.
 * @param {Number} videoId the video ID
 * @param {String} content the content in string base 64
 * @return {Array} an array containing the result code and the json to send.
 */
async function handleVideo(videoId, content) {
    if (videoId == null && content == null) {
        return [400, { 'error': 'Missing arguments' }];
    } else {
        const transcript = await getSTT(content);
        sttDAO.insertVideoTranscript(videoId, transcript);
        return [201, {}];
    }
}

/**
 * Gets the video transcript for a video.
 * @param {Number} videoId the video ID.
 * @return {Promise<*[]>} The response to send.
 */
async function getVideoTranscript(videoId) {
    if (videoId == null) {
        return [400, { 'error': 'Missing arguments' }];
    } else {
        const transcript = await sttDAO.getVideoTranscript(videoId);
        return [200, transcript];
    }
}

export default {
    handleVideo,
    getVideoTranscript,
};
