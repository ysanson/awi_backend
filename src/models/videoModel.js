import mongoose from 'mongoose';

const videoSchema = new mongoose.Schema({
    videoId: Number,
    duration: Number,
    name: String,
    email: String,
    order: Number,
    courseId: Number,
    transcript: [
        {
            line: String,
            beginningTimestamp: Number,
        },
    ],
});

const Video = mongoose.model('Video', videoSchema);

export default Video;
