import mongoose from 'mongoose';

const courseSchema = new mongoose.Schema({
    creatorId: Number,
    courseName: String,
    description: String,
    image: String,
    access: Boolean,
    password: String,
    isSynchro: Boolean,
    driveFolder: String,
    videos: [
        {
            videoId: Number,
        },
    ],
});

const Course = mongoose.model('Course', courseSchema);

export default Course;
