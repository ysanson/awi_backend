import courseModel from './courseModel';
import userModel from './userModel';
import videoModel from './videoModel';

export default {
    courseModel,
    userModel,
    videoModel,
};
