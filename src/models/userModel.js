import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    id: Number,
    lastName: String,
    firstName: String,
    email: String,
    isTeacher: Boolean,
    studentClass: String,
    courses: [
        {
            courseId: Number,
            courseName: String,
            coursePassword: String,
            isCreator: Boolean,
        },
    ],
    watch: [
        {
            watchedTime: Number,
            videoId: Number,
        },
    ],
});

const User = mongoose.model('User', userSchema);

export default User;
