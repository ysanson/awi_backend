"use strict";
require('dotenv').config();
const mongoose = require('mongoose');
const assert = require('assert');

describe('Database Tests', function() {

    it('Should connect to the database.', function(done) {
        let res;
        mongoose.connect(process.env.MONGO_CONN, {
            poolSize: 100,
            socketTimeoutMS: 0,
            keepAlive: true,
            reconnectTries: 30,
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        const db = mongoose.connection;
        db.on('error', res = function() {
            console.error.bind(console, 'connection error');
            return false;
        });
        db.once('open', res = function() {
            console.log('We are connected to test database!');
            return true;
        });
        assert(res);
        done();
    });
});
